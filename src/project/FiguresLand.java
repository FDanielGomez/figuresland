/*
* Project created to Computer Graphics class
* @author Francisco Daniel Gomez Alvarez
*/
package project;

import javax.swing.*;
import com.sun.j3d.utils.behaviors.keyboard.KeyNavigatorBehavior;
import com.sun.j3d.utils.geometry.ColorCube;
import com.sun.j3d.utils.pickfast.behaviors.PickRotateBehavior;
import com.sun.j3d.utils.universe.SimpleUniverse;
import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import java.awt.*;

public class FiguresLand extends JPanel
{
    BoundingSphere bounding = new BoundingSphere();
    public FiguresLand()
    {
        setLayout(new BorderLayout());
        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
        Canvas3D canvas3D = new Canvas3D(config);
        add("Center", canvas3D);
        SimpleUniverse simpleU = new SimpleUniverse(canvas3D);
        BranchGroup scene = createSceneGraph(simpleU,canvas3D);
        simpleU.addBranchGraph(scene);
    }

    public Shape3D createLand()
    {
        LineArray landGeom = new LineArray(44, GeometryArray.COORDINATES | GeometryArray.COLOR_3);
        float l = -50.0f;
        for(int c = 0; c < 44; c+=4)
        {
            landGeom.setCoordinate( c+0, new Point3f( -50.0f, 0.0f,  l ));
            landGeom.setCoordinate( c+1, new Point3f(  50.0f, 0.0f,  l ));
            landGeom.setCoordinate( c+2, new Point3f(   l   , 0.0f, -50.0f ));
            landGeom.setCoordinate( c+3, new Point3f(   l   , 0.0f,  50.0f ));
            l += 10.0f;
        }
        Color3f c = new Color3f(0.1f, 0.8f, 0.1f);
        for(int i = 0; i < 44; i++) landGeom.setColor( i, c);
        return new Shape3D(landGeom);
    }

    public BranchGroup createSceneGraph(SimpleUniverse su,Canvas3D canvas)
    {
        // Create the root of the branch graph
        TransformGroup vpTrans = null;
        BranchGroup objRoot = new BranchGroup();
        Vector3f translate = new Vector3f();
        Transform3D T3D = new Transform3D();
        TransformGroup TG = null;
        objRoot.addChild(createLand());
        SharedGroup share = new SharedGroup();

        Background background = createBackground();
        PointLight pointLight = createPointLight1();
        PointLight pointLight2 = createPointLight2();
        AmbientLight ambientLight = createAmbientLight();
        objRoot.addChild(background);
        objRoot.addChild(pointLight);
        objRoot.addChild(pointLight2);
        objRoot.addChild(ambientLight);

        /////////////////////////////////////////////////////////////////////////
        BoundingSphere behaveBounds = new BoundingSphere();
        TransformGroup objRotate = createBoxTransformGroup(-0.6f, 0.7f, -0.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        //objRotate.addChild(new Box(1.0f,1.0f,1.0f,new Appearance()));
        objRoot.addChild(objRotate);
        PickRotateBehavior pickRotate = new PickRotateBehavior(objRoot, canvas, behaveBounds);
        objRoot.addChild(pickRotate);

        objRotate = createBoxTransformGroup(9.6f,0.7f,2.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        objRoot.addChild(objRotate);
        pickRotate = new PickRotateBehavior(objRoot, canvas, behaveBounds);
        objRoot.addChild(pickRotate);

        objRotate = createBoxTransformGroup(12.6f,0.7f,18.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        objRoot.addChild(objRotate);
        pickRotate = new PickRotateBehavior(objRoot, canvas, behaveBounds);
        objRoot.addChild(pickRotate);

        objRotate = createBoxTransformGroup(-17.6f,0.7f,-24.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        objRoot.addChild(objRotate);
        pickRotate = new PickRotateBehavior(objRoot, canvas, behaveBounds);
        objRoot.addChild(pickRotate);

        objRotate = createBoxTransformGroup(-30.6f,0.7f,35.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        objRoot.addChild(objRotate);
        pickRotate = new PickRotateBehavior(objRoot, canvas, behaveBounds);
        objRoot.addChild(pickRotate);

        //////////////////////////////////////////////////////////////////////////

        vpTrans = su.getViewingPlatform().getViewPlatformTransform();
        translate.set( 0.0f, 0.3f, 0.0f);
        T3D.setTranslation(translate);
        vpTrans.setTransform(T3D);
        KeyNavigatorBehavior keyNavBeh = new KeyNavigatorBehavior(vpTrans);
        keyNavBeh.setSchedulingBounds(new BoundingSphere(new Point3d(),1000.0));
        objRoot.addChild(keyNavBeh);
        objRoot.compile();
        return objRoot;
    }

    private Background createBackground()
    {
        Background background = new Background();
        background.setApplicationBounds(bounding);
        return background;
    }

    private PointLight createPointLight1()
    {
        PointLight light = new PointLight(new Color3f(Color.white), new Point3f(-0.6f, 1.0f, -0.6f), new Point3f(1f,0f,0f));
        light.setInfluencingBounds(bounding);
        return light;
    }

    private PointLight createPointLight2()
    {
        PointLight light = new PointLight(new Color3f(Color.white), new Point3f(9.6f,1.0f,2.6f), new Point3f(1f,0f,0f));
        light.setInfluencingBounds(bounding);
        return light;
    }

    private AmbientLight createAmbientLight()
    {
        AmbientLight light = new AmbientLight(true, new Color3f(Color.white));
        light.setInfluencingBounds(bounding);
        return light;
    }


    private TransformGroup createBoxTransformGroup(float x,float y,float z)
    {
        Appearance appearance = new Appearance();
        appearance.setMaterial(new Material());
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        tG.addChild(new ColorCube());
        return tG;
    }

    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        frame.add(new FiguresLand());
        frame.add(new FiguresLand());
        frame.setSize(1000,600);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
